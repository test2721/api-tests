var bottlejs = require('bottlejs').pop('test');

bottlejs.factory('PageObject', function () {

    return {
        getMediumApi: () => {
            const MediumPage = require('./medium-api/medium-api.po');
            return new MediumPage();
        }
    };
});

module.exports = bottlejs;
