'use strict';

const frisby = require('frisby');

class MediumApi {

    //--------------------------------------------------------------------------
    // Functions
    //--------------------------------------------------------------------------

    async makeGetRequest(url, options) {
        return await frisby.timeout(60000)
            .get(url, options)
            .then(async (res) => {
                return res;
            });
    };

    async makePostRequest(url, options) {
        return await frisby.timeout(60000)
            .post(url, options)
            .then(async (res) => {
                return res;
            });
    };
}

module.exports = MediumApi;
