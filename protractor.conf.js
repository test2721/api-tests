exports.config = {

    directConnect: true,

    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            args: ['headless'],
        }
    },

    plugins: [{
        package: 'protractor-console',
        logLevels: ['severe'],
        path: 'node_modules/protractor-console'
    }],

    getPageTimeout: 60000,
    allScriptsTimeout: 60000,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    suites: {
        all: ['features/*.feature']
    },
    cucumberOpts: {
        // require step definitions
        require: [
            'features/step_definitions/*.js',
            'features/support/cucumber-json-report.js',
            'features/support/cucumber-html-report.js',
            'features/support/cucumber-junit-report.js',
        ],
    },

    onPrepare: async function () {
        const {Given, Then, When} = require('cucumber');
        global.Given = Given;
        global.When = When;
        global.Then = Then;
    },
};
