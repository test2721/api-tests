const pageObject = require('../../page_objects/pages.js').container.PageObject;
const medium = pageObject.getMediumApi();
const chai = require('chai');
const assert = chai.assert;

module.exports = async function() {

    const token = '29f37a2d6189fb6b31bc12f92e0dacaedbdeb8b7b3bd3f7933d76d9fea1f48aa7'; // This is generated stable token for Medium account.

    var headers = { headers: {} },
        response;

    this.When(/^I make (.*) request for (.*) endpoint$/, async function (request, endpoint) {
        let options = headers;

        switch (request) {
            case 'GET':
                response = await medium.makeGetRequest(endpoint, options);
                break;
            case 'POST':
                response = await medium.makePostRequest(endpoint, options);
                break;
        }
    });

    this.When(/^I setup headers:$/, {timeout: 60000}, async function (table) {
        const result = table.hashes();

        if (result[0].token === 'true') Object.assign(headers['headers'],{ 'Authorization': 'Bearer ' + token }); else
            Object.assign(headers['headers'],{ 'Authorization': 'Bearer ' + result[0].token });

        if (result[0]['content-type'] !== 'false' && result[0]['content-type'] !== undefined) Object.assign(headers['headers'],{ 'Content-type': result[0]['content-type']}); else
            delete headers.headers['Content-type'];
    });

    this.Then(/^I verify response:$/, async function (table) {
        const result = table.hashes();
        let status, body;

        if (result[0].status) { status = parseInt(result[0].status); assert.deepEqual(status, response.status); }
        if (result[0].body !== 'false' && result[0].body !== undefined) { body = result[0].body; assert.include(response.body, body); }
    });

};
