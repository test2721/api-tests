Feature: API testing

  Scenario Outline: Medium - <scenarioDescription>
    When I setup headers:
      | token        |
      | <tokenValue> |
    And I make <requestType> request for <endpoint> endpoint
    Then I verify response:
      | status          | body   |
      | <respStatus>    | <body> |

  Examples:
    | tokenValue | requestType | endpoint                     | respStatus | body                                                                                                                                                     | scenarioDescription                                        |
    | true       | GET         | https://api.medium.com/v1/me | 200        | "id":"19a07f378b304cdd3f868f90a1fbad186981685832379deb989e1f17f6289f57e","username":"8v.kupriy","name":"Vida Vida","url":"https://medium.com/@8v.kupriy" | Verify [v1/me] endpoint                                    |
    | false      | GET         | https://api.medium.com/v1/me | 401        | false                                                                                                                                                    | Verify 401 unauthorized for [v1/me] endpoint               |
# This scenario should be failed intentionally to show the crashed scenario in the report
    | true       | POST        | https://api.medium.com/v1/me | 200        | false                                                                                                                                                    | Verify 405 POST method is not allowed for [v1/me] endpoint |

