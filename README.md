## Running tests

### Installation of environment
Run `npm install`
Run `./node_modules/protractor/bin/webdriver-manager update`

### Tests execution

Run specific suite: `./node_modules/protractor/bin/protractor protractor.conf.js --suite=all`


### Checking reports

You can check report after running scenarios and opening cucumber_report.html
